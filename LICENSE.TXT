GNU Lesser General Public License (LGPL) version 2 or later

This is a python compiler framework for guile of a python version made to
interoperate well with guile

For The rest, COPYING.TXT applies
  Copyright (c) Stefan Israelsson Tampe.

See "COPYING.LIB" for more information about the license.
