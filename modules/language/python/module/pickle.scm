(define-module (language python module pickle)
  #:use-module (language python persist)
  #:export (dump dumps load loads name nameDeep))

(define* (dump obj file #:key (protocol #f) (fix_imports #t))
  ((@@ (persist persistance) dump) obj file))

(define* (dumps obj #:key (protocol #f) (fix_imports #t))
  ((@@ (persist persistance) dumps) obj))

(define* (load file
	       #:key (fix_imports #t) (encodeing "ASCII") (errors "strict"))
  ((@@ (persist persistance) load) file))

(define* (loads s
	       #:key (fix_imports #t) (encodeing "ASCII") (errors "strict"))
  ((@@ (persist persistance) loads) s))

(define-syntax-rule (name x)     (name-object      x))
(define-syntax-rule (nameDeep x) (name-object-deep x))
