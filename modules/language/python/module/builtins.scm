(define-module (language python module builtins)
  #:use-module (language python module python))

(define-syntax re-export-all
  (syntax-rules ()
    [(_ iface)
     (module-for-each 
      (lambda (name . l)
        (module-re-export! (current-module) ((@ (guile) list) name)))
      (resolve-interface 'iface))]
    [(_ iface _ li)
     (let ((l 'li))
       (module-for-each 
        (lambda (name . l)        
          (if (not (member name l))
              (module-re-export! (current-module) ((@ (guile) list) name))))
        (resolve-interface 'iface)))]))

(re-export-all (language python module python))
  
