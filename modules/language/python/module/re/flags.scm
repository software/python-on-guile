(define-module (language python module re flags)
  #:export (set-flags get-flags *flags*
		      A ASCII DEBUG I IGNORECASE L LOCALE M MULTILINE
                      X VERBOSE S DOTALL))

(define *flags* (make-fluid 0))
(define (set-flags x) (fluid-set! *flags* x))
(define (get-flags)   (fluid-ref *flags*))

(define A          1)
(define ASCII      A)

(define DEBUG      2)

(define I          4)
(define IGNORECASE I)

(define L          8)
(define LOCALE     L)

(define M          16)
(define MULTILINE  M)

(define X          32)
(define VERBOSE    X)

(define S          64)
(define DOTALL     X)

