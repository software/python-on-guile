(define-module (language python module keyword)
  #:use-module (language python string)
  #:use-module (language python set)
  #:use-module (oop pf-objects)
  
  #:export (kwlist iskeyword))

(define kwlist
  '("False"
    "None"
    "True"
    "and"
    "as"
    "assert"
    "break"
    "class"
    "continue"
    "def"
    "del"
    "elif"
    "else"
    "except"
    "finally"
    "for"
    "from"
    "global"
    "if"
    "import"
    "in"
    "is"
    "lambda"
    "nonlocal"
    "not"
    "or"
    "pass"
    "raise"
    "return"
    "try"
    "while"
    "with"
    "yield"))


(define iskeyword (ref (py-set kwlist) '__contains__))
  
    
