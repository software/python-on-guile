(define-module (language python module warnings)
  #:export (warn))

(define (warn . l) ((@ (guile) warn) l))
