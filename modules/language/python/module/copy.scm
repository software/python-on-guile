(define-module (language python module copy)
  #:use-module (ice-9 match)
  #:export (Error copy deepcopy))

(define Error 'CopyError)

(define (s x)
  (match x
    ((#:obj x) x)
    (x x)))
(define (copy     x) (s ((@@ (persist persistance)      copy) x)))
(define (deepcopy x) (s ((@@ (persist persistance) deep-copy) x)))
