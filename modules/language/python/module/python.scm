(define-module (language python module python )
  #:use-module (language python module _python)
  #:use-module (language python compile       )
  #:use-module (language python module        )
  #:use-module ((language python module string) #:select ())
  #:use-module (language python memoryview    )
  #:use-module ((oop pf-objects) #:select (define-python-class))
  #:use-module ((language python format2) #:select ())
  #:re-export (memoryview)
  #:export (ClassMethod StaticMethod Funcobj))

(define-syntax re-export-all
  (syntax-rules ()
    [(_ iface)
     (module-for-each 
      (lambda (name . l)
        (module-re-export! (current-module)
			   ((@ (guile) list) name)))
      (module-public-interface (resolve-module 'iface)))]
    [(_ iface _ li)
     (let ((l 'li))
       (module-for-each 
        (lambda (name . l)
          (if (not (member name l))
              (module-re-export! (current-module)
                                 ((@ (guile) list) (pk name)))))
        (module-public-interface (resolve-module 'iface))))]))

(set! (@@ (language python format2) splitm)
  (@@ (language python module re) splitm))

(set! (@@ (language python format2) splitmm)
  (@@ (language python module re) splitmm))

(set! (@ (language python module os) path)
  (Module '(path os module python language) '(path os)))

(re-export-all (language python module _python))

(set! (@@ (language python eval) MM) (@@ (language python compile) void))

(define-python-class ClassMethod  ())
(define-python-class StaticMethod ())
(define-python-class Funcobj      ())
