(define-module (language python module stat)
  #:use-module (language python list)
  #:use-module (language python string)
  #:use-module (language python for)
  #:export (ST_MODE ST_INO ST_DEV ST_NLINK ST_UID ST_GID ST_SIZE ST_ATIME
                    ST_MTIME ST_CTIME S_ISUID S_ISGID S_ENFMT S_ISVTX S_IREAD
                    S_IWRITE S_IEXEC S_IRWXU S_IRUSR S_IWUSR S_IXUSR S_IRGRP
                    S_IWGRP S_IXGRP S_IRWXG S_IROTH S_IWOTH S_IXOTH S_IRWXO
                    S_IFDIR S_IFREG S_IFLNK S_IFCHR S_IFBLK S_IFIFO S_IFSOC
                    UF_NODUMP UF_IMMUTABLE UF_APPEND UF_OPAQUE UF_NOUNLINK
                    UF_COMPRESSED UF_HIDDEN SF_ARCHIVED SF_IMMUTABLE SF_APPEND
                    SF_NOUNLINK SF_SNAPSHOT S_ISDIR S_ISREG S_ISLNK S_ISCHR
                    S_ISBLK S_ISFIFO S_ISSOCK S_IMODE S_IFMT
                    filemode))

(define ST_MODE  0)
(define ST_INO   1)
(define ST_DEV   2)
(define ST_NLINK 3)
(define ST_UID   4)
(define ST_GID   5)
(define ST_SIZE  6)
(define ST_ATIME 7)
(define ST_MTIME 8)
(define ST_CTIME 9)

(define S_ISUID  #o04000)
(define S_ISGID  #o02000)
(define S_ENFMT  S_ISGID)
(define S_ISVTX  #o01000)
(define S_IREAD  #o00400)
(define S_IWRITE #o00200)
(define S_IEXEC  #o00100)
(define S_IRWXU  (logior S_IEXEC S_IWRITE S_IREAD))
(define S_IRUSR  S_IREAD)
(define S_IWUSR  S_IWRITE)
(define S_IXUSR  S_IEXEC)
(define S_IRGRP  #o00040)
(define S_IWGRP  #o00020)
(define S_IXGRP  #o00010)
(define S_IRWXG  (logior S_IXGRP S_IWGRP S_IRGRP))
(define S_IROTH  #o00004)
(define S_IWOTH  #o00002)
(define S_IXOTH  #o00001)
(define S_IRWXO  (logior S_IXOTH S_IWOTH S_IROTH))

;;Internal
(define SS_IFMT   #o170000)


(define S_IFDIR   #o040000)
(define S_IFREG   #o100000)
(define S_IFLNK   #o120000)
(define S_IFCHR   #o020000)
(define S_IFBLK   #o060000)
(define S_IFIFO   #o010000)
(define S_IFSOCK  #o140000)

(define (S_ISDIR  x) (= (logand x SS_IFMT) S_IFDIR))
(define (S_ISREG  x) (= (logand x SS_IFMT) S_IFREG))
(define (S_ISLNK  x) (= (logand x SS_IFMT) S_IFLNK))
(define (S_ISCHR  x) (= (logand x SS_IFMT) S_IFCHR))
(define (S_ISBLK  x) (= (logand x SS_IFMT) S_IFBLK))
(define (S_ISFIFO x) (= (logand x SS_IFMT) S_IFIFO))
(define (S_ISSOCK x) (= (logand x SS_IFMT) S_IFSOCK))
(define (S_IMODE  x) (logand x #o7777))
(define (S_IFMT   x) (logand x SS_IFMT))

(define UF_NODUMP      #x00000001)
(define UF_IMMUTABLE   #x00000002)
(define UF_APPEND      #x00000004)
(define UF_OPAQUE      #x00000008)
(define UF_NOUNLINK    #x00000010)
(define UF_COMPRESSED  #x00000020)
(define UF_HIDDEN      #x00008000)
(define SF_ARCHIVED    #x00010000)
(define SF_IMMUTABLE   #x00020000)
(define SF_APPEND      #x00040000)
(define SF_NOUNLINK    #x00100000)
(define SF_SNAPSHOT    #x00200000)

(define _filemode_table
  `(((,S_IFLNK         "l")
     (,S_IFREG         "-")
     (,S_IFBLK         "b")
     (,S_IFDIR         "d")
     (,S_IFCHR         "c")
     (,S_IFIFO         "p"))

    ((,S_IRUSR         "r"))
    ((,S_IWUSR         "w"))
    ((,(logior S_IXUSR S_ISUID) "s")
     (,S_ISUID         "S")
     (,S_IXUSR         "x"))

    ((,S_IRGRP         "r"))
    ((,S_IWGRP         "w"))
    ((,(logior S_IXGRP S_ISGID) "s")
     (,S_ISGID         "S")
     (,S_IXGRP         "x"))

    ((,S_IROTH         "r"))
    ((,S_IWOTH         "w"))
    ((,(logior S_IXOTH S_ISVTX) "t")
     (,S_ISVTX         "T")
     (,S_IXOTH         "x"))))

(define (filemode mode)
  """Convert a file's mode to a string of the form '-rwxrwxrwx'."""
  (define perm (py-list))
  
  (for ((table : _filemode_table)) ()
       (for ((bit char : table)) ()
            (if (= (logand mode  bit) bit)
                (begin
                  (pylist-append! perm char)
                  (break)))
            #:final
            (pylist-append! perm "-")))
  (py-join "" perm))

