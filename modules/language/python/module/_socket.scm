(define-module (language python module _socket)
  #:use-module (language python string)
  #:use-module (language python yield)
  #:use-module (language python bytes)
  #:use-module (language python exceptions)
  #:use-module (language python def)
  #:use-module (language python list)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)

  #:use-module (oop pf-objects)
  #:export
  (SOCK_CLOEXEC SOCK_NONBLOCK SOCK_RDM SOCK_STREAM
                SOCK_DGRAM SOCK_RAW SOCK_SEQPACKET

                MSG_CMSG_CLOEXEC MSG_CONFIRM MSG_CTRUNC MSG_DONTROUTE
                MSG_DONTWAIT MSG_EOR MSG_ERRQUEUE MSG_FASTOPEN MSG_MORE
                MSG_NOSIGNAL MSG_OOB MSG_PEEK MSG_TRUNC MSG_WAITALL
                MSG_DONTROUTE MSG_DONTWAIT MSG_OOB MSG_PEEK

                AI_ADDRCONFIG AI_CANONNAME AI_NUMERICSERV AI_V4MAPPED
                AI_ALL AI_NUMERICHOST AI_PASSIVE

                IP_ADD_MEMBERSHIP IP_DEFAULT_MULTICAST_LOOP
                IP_DEFAULT_MULTICAST_TTL IP_DROP_MEMBERSHIP IP_HDRINCL
                IP_MAX_MEMBERSHIPS IP_MULTICAST_IF IP_MULTICAST_LOOP
                IP_MULTICAST_TTL IP_OPTIONS IP_RECVOPTS IP_RECVRETOPTS
                IP_RETOPTS IP_TOS IP_TRANSPARENT IP_TTL

                IPV6_CHECKSUM IPV6_DONTFRAG IPV6_DSTOPTS IPV6_HOPLIMIT
                IPV6_HOPOPTS IPV6_JOIN_GROUP IPV6_LEAVE_GROUP
                IPV6_MULTICAST_HOPS IPV6_MULTICAST_IF IPV6_MULTICAST_LOOP
                IPV6_NEXTHOP IPV6_PATHMTU IPV6_PKTINFO IPV6_RECVDSTOPTS
                IPV6_RECVHOPLIMIT IPV6_RECVHOPOPTS IPV6_RECVPATHMTU
                IPV6_RECVPKTINFO IPV6_RECVRTHDR IPV6_RECVTCLASS
                IPV6_RTHDR IPV6_RTHDRDSTOPTS IPV6_RTHDR_TYPE_0 IPV6_TCLASS
                IPV6_UNICAST_HOPS IPV6_V6ONLY

                IPPROTO_AH IPPROTO_DSTOPTS IPPROTO_EGP IPPROTO_ESP
                IPPROTO_FRAGMENT IPPROTO_GRE IPPROTO_HOPOPTS IPPROTO_ICMP
                IPPROTO_ICMPV6 IPPROTO_IDP IPPROTO_IGMP IPPROTO_IP
                IPPROTO_IPIP IPPROTO_IPV6 IPPROTO_NONE IPPROTO_PIM
                IPPROTO_PUP IPPROTO_RAW IPPROTO_ROUTING IPPROTO_RSVP
                IPPROTO_SCTP IPPROTO_TCP IPPROTO_TP IPPROTO_UDP

                INADDR_ALLHOSTS_GROUP INADDR_ANY INADDR_BROADCAST
                INADDR_LOOPBACK INADDR_MAX_LOCAL_GROUP INADDR_NONE
                INADDR_UNSPEC_GROUP

                TCP_CONGESTION TCP_CORK TCP_DEFER_ACCEPT TCP_FASTOPEN
                TCP_INFO TCP_KEEPCNT TCP_KEEPIDLE TCP_KEEPINTVL
                TCP_LINGER2 TCP_MAXSEG TCP_NODELAY TCP_QUICKACK TCP_SYNCNT
                TCP_USER_TIMEOUT TCP_WINDOW_CLAMP
                
                TIPC_ADDR_ID TIPC_ADDR_NAME TIPC_ADDR_NAMESEQ TIPC_CFG_SRV
                TIPC_CLUSTER_SCOPE TIPC_CONN_TIMEOUT
                TIPC_CRITICAL_IMPORTANCE TIPC_DEST_DROPPABLE
                TIPC_HIGH_IMPORTANCE TIPC_IMPORTANCE TIPC_LOW_IMPORTANCE
                TIPC_MEDIUM_IMPORTANCE TIPC_NODE_SCOPE TIPC_PUBLISHED
                TIPC_SRC_DROPPABLE TIPC_SUBSCR_TIMEOUT TIPC_SUB_CANCEL
                TIPC_SUB_PORTS TIPC_SUB_SERVICE TIPC_TOP_SRV
                TIPC_WAIT_FOREVER TIPC_WITHDRAWN TIPC_ZONE_SCOPE
                
                SO_ACCEPTCONN SO_BINDTODEVICE SO_BROADCAST SO_DEBUG
                SO_DOMAIN SO_DONTROUTE SO_ERROR SO_KEEPALIVE SO_LINGER
                SO_MARK SO_OOBINLINE SO_PASSCRED SO_PASSSEC SO_PEERCRED
                SO_PEERSEC SO_PRIORITY SO_PROTOCOL SO_RCVBUF SO_RCVLOWAT
                SO_RCVTIMEO SO_REUSEADDR SO_REUSEPORT SO_SNDBUF
                SO_SNDLOWAT SO_SNDTIMEO SO_TYPE
                
                SOL_ALG SOL_CAN_BASE SOL_CAN_RAW SOL_HCI SOL_IP SOL_RDS
                SOL_SOCKET SOL_TCP SOL_TIPC SOL_UDP
                
                SOMAXCONN
                
                CMSG_LEN CMSG_SPACE
                    
                HCI_DATA_DIR HCI_FILTER HCI_TIME_STAMP

                EAI_ADDRFAMILY EAI_AGAIN EAI_BADFLAGS EAI_FAIL EAI_FAMILY
                EAI_MEMORY EAI_NODATA EAI_NONAME EAI_OVERFLOW EAI_SERVICE
                EAI_SOCKTYPE EAI_SYSTEM

                CAN_BCM CAN_BCM_RX_CHANGED CAN_BCM_RX_DELETE
                CAN_BCM_RX_READ CAN_BCM_RX_SETUP CAN_BCM_RX_STATUS
                CAN_BCM_RX_TIMEOUT CAN_BCM_TX_DELETE CAN_BCM_TX_EXPIRED
                CAN_BCM_TX_READ CAN_BCM_TX_SEND CAN_BCM_TX_SETUP
                CAN_BCM_TX_STATUS CAN_EFF_FLAG CAN_EFF_MASK CAN_ERR_FLAG
                CAN_ERR_MASK CAN_RAW CAN_RAW_ERR_FILTER CAN_RAW_FD_FRAMES
                CAN_RAW_FILTER CAN_RAW_LOOPBACK CAN_RAW_RECV_OWN_MSGS
                CAN_RTR_FLAG CAN_SFF_MASK
                    
                BTPROTO_HCI BTPROTO_L2CAP BTPROTO_RFCOMM BTPROTO_SCO
                    
                BDADDR_ANY BDADDR_LOCAL

                ALG_OP_DECRYPT ALG_OP_ENCRYPT ALG_OP_SIGN ALG_OP_VERIFY
                ALG_SET_AEAD_ASSOCLEN ALG_SET_AEAD_AUTHSIZE ALG_SET_IV
                ALG_SET_KEY ALG_SET_OP ALG_SET_PUBKEY
                
                AF_ALG AF_APPLETALK AF_ASH AF_ATMPVC AF_ATMSVC AF_AX25
                AF_BLUETOOTH AF_BRIDGE AF_CAN AF_DECnet AF_ECONET AF_INET
                AF_INET6 AF_IPX AF_IRDA AF_KEY AF_LLC AF_NETBEUI
                AF_NETLINK AF_NETROM AF_PACKET AF_PPPOX AF_RDS AF_ROSE
                AF_ROUTE AF_SECURITY AF_SNA AF_TIPC AF_UNIX AF_UNSPEC
                AF_WANPIPE AF_X25
                
                SHUT_RD SHUT_RDWR SHUT_WR
                
                SCM_CREDENTIALS SCM_RIGHTS
                    
                PF_CAN PF_PACKET PF_RDS
                    
                PACKET_BROADCAST PACKET_FASTROUTE PACKET_HOST
                PACKET_LOOPBACK PACKET_MULTICAST PACKET_OTHERHOST
                PACKET_OUTGOING

                NI_DGRAM NI_MAXHOST NI_MAXSERV NI_NAMEREQD NI_NOFQDN
                NI_NUMERICHOST NI_NUMERICSERV
                
                NETLINK_CRYPTO NETLINK_DNRTMSG NETLINK_FIREWALL
                NETLINK_IP6_FW NETLINK_NFLOG NETLINK_ROUTE
                NETLINK_USERSOCK NETLINK_XFRM
                
                socket

                dup getaddrinfo getdefaulttimeout gethostbyname
                gethostbyname_ex gethostname getnameinfo getprotobyn
                getservbyname htonl htons if_indextoname if_nameindex
                if_nametoindex inet_aton inet_ntop ntohl setdefaulttimeout
                getservbyport gethostbyaddr

                error timeout
                ))

(define-syntax-rule (aif it p . l) (let ((it p)) (if it . l)))

(define-syntax-rule (wrap self it code ...)
  (aif it (ref self '__fileno)
       (begin code ...)
       (raise (ValueError "no socket created"))))

(define NETLINK_CRYPTO 21)
(define NETLINK_DNRTMSG 14)
(define NETLINK_FIREWALL 3)
(define NETLINK_IP6_FW 13)
(define NETLINK_NFLOG 5)
(define NETLINK_ROUTE 0)
(define NETLINK_USERSOCK 2)
(define NETLINK_XFRM 6)

(define NI_DGRAM 16)
(define NI_MAXHOST 1025)
(define NI_MAXSERV 32)
(define NI_NAMEREQD 8)
(define NI_NOFQDN 4)
(define NI_NUMERICHOST 1)
(define NI_NUMERICSERV 2)

(define PACKET_BROADCAST 1)
(define PACKET_FASTROUTE 6)
(define PACKET_HOST 0)
(define PACKET_LOOPBACK 5)
(define PACKET_MULTICAST 2)
(define PACKET_OTHERHOST 3)
(define PACKET_OUTGOING 4)

(define PF_CAN 29)
(define PF_PACKET 17)
(define PF_RDS 21)

(define SCM_CREDENTIALS 2)
(define SCM_RIGHTS 1)


(define SHUT_RD 0)
(define SHUT_RDWR 2)
(define SHUT_WR 1)

(define AF_ALG 38)
(define AF_APPLETALK 5)
(define AF_ASH 18)
(define AF_ATMPVC 8)
(define AF_ATMSVC 20)
(define AF_AX25 3)
(define AF_BLUETOOTH 31)
(define AF_BRIDGE 7)
(define AF_CAN 29)
(define AF_DECnet 12)
(define AF_ECONET 19)
(define AF_INET 2)
(define AF_INET6 10)
(define AF_IPX 4)
(define AF_IRDA 23)
(define AF_KEY 15)
(define AF_LLC 26)
(define AF_NETBEUI 13)
(define AF_NETLINK 16)
(define AF_NETROM 6)
(define AF_PACKET 17)
(define AF_PPPOX 24)
(define AF_RDS 21)
(define AF_ROSE 11)
(define AF_ROUTE 16)
(define AF_SECURITY 14)
(define AF_SNA 22)
(define AF_TIPC 30)
(define AF_UNIX 1)
(define AF_UNSPEC 0)
(define AF_WANPIPE 25)
(define AF_X25 9)

(define ALG_OP_DECRYPT 0)
(define ALG_OP_ENCRYPT 1)
(define ALG_OP_SIGN 2)
(define ALG_OP_VERIFY 3)
(define ALG_SET_AEAD_ASSOCLEN 4)
(define ALG_SET_AEAD_AUTHSIZE 5)
(define ALG_SET_IV 2)
(define ALG_SET_KEY 1)
(define ALG_SET_OP 3)
(define ALG_SET_PUBKEY 6)

(define BDADDR_ANY   "00:00:00:00:00:00")
(define BDADDR_LOCAL "00:00:00:FF:FF:FF")

(define BTPROTO_HCI 1)
(define BTPROTO_L2CAP 0)
(define BTPROTO_RFCOMM 3)
(define BTPROTO_SCO 2)

(define CAN_BCM 2)
(define CAN_BCM_RX_CHANGED 12)
(define CAN_BCM_RX_DELETE 6)
(define CAN_BCM_RX_READ 7)
(define CAN_BCM_RX_SETUP 5)
(define CAN_BCM_RX_STATUS 10)
(define CAN_BCM_RX_TIMEOUT 11)
(define CAN_BCM_TX_DELETE 2)
(define CAN_BCM_TX_EXPIRED 9)
(define CAN_BCM_TX_READ 3)
(define CAN_BCM_TX_SEND 4)
(define CAN_BCM_TX_SETUP 1)
(define CAN_BCM_TX_STATUS 8)
(define CAN_EFF_FLAG 2147483648)
(define CAN_EFF_MASK 536870911)
(define CAN_ERR_FLAG 536870912)
(define CAN_ERR_MASK 536870911)
(define CAN_RAW 1)
(define CAN_RAW_ERR_FILTER 2)
(define CAN_RAW_FD_FRAMES 5)
(define CAN_RAW_FILTER 1)
(define CAN_RAW_LOOPBACK 3)
(define CAN_RAW_RECV_OWN_MSGS 4)
(define CAN_RTR_FLAG 1073741824)
(define CAN_SFF_MASK 2047)

(define (CMSG_LEN . l)
  (error "CMSG_LEN not implemented"))
(define (CMSG_SPACE . l)
  (error "CMSG_SPACE not implemented"))

(define EAI_ADDRFAMILY -9)
(define EAI_AGAIN -3)
(define EAI_BADFLAGS -1)
(define EAI_FAIL -4)
(define EAI_FAMILY -6)
(define EAI_MEMORY -10)
(define EAI_NODATA -5)
(define EAI_NONAME -2)
(define EAI_OVERFLOW -12)
(define EAI_SERVICE -8)
(define EAI_SOCKTYPE -7)
(define EAI_SYSTEM -11)

(define HCI_DATA_DIR 1)
(define HCI_FILTER 2)
(define HCI_TIME_STAMP 3)

(define SOMAXCONN 128)

(define SOL_ALG 279)
(define SOL_CAN_BASE 100)
(define SOL_CAN_RAW 101)
(define SOL_HCI 0)
(define SOL_IP 0)
(define SOL_RDS 276)
(define SOL_SOCKET 1)
(define SOL_TCP 6)
(define SOL_TIPC 271)
(define SOL_UDP 17)

(define SO_ACCEPTCONN 30)
(define SO_BINDTODEVICE 25)
(define SO_BROADCAST 6)
(define SO_DEBUG 1)
(define SO_DOMAIN 39)
(define SO_DONTROUTE 5)
(define SO_ERROR 4)
(define SO_KEEPALIVE 9)
(define SO_LINGER 13)
(define SO_MARK 36)
(define SO_OOBINLINE 10)
(define SO_PASSCRED 16)
(define SO_PASSSEC 34)
(define SO_PEERCRED 17)
(define SO_PEERSEC 31)
(define SO_PRIORITY 12)
(define SO_PROTOCOL 38)
(define SO_RCVBUF 8)
(define SO_RCVLOWAT 18)
(define SO_RCVTIMEO 20)
(define SO_REUSEADDR 2)
(define SO_REUSEPORT 15)
(define SO_SNDBUF 7)
(define SO_SNDLOWAT 19)
(define SO_SNDTIMEO 21)
(define SO_TYPE 3)

(define TIPC_ADDR_ID 3)
(define TIPC_ADDR_NAME 2)
(define TIPC_ADDR_NAMESEQ 1)
(define TIPC_CFG_SRV 0)
(define TIPC_CLUSTER_SCOPE 2)
(define TIPC_CONN_TIMEOUT 130)
(define TIPC_CRITICAL_IMPORTANCE 3)
(define TIPC_DEST_DROPPABLE 129)
(define TIPC_HIGH_IMPORTANCE 2)
(define TIPC_IMPORTANCE 127)
(define TIPC_LOW_IMPORTANCE 0)
(define TIPC_MEDIUM_IMPORTANCE 1)
(define TIPC_NODE_SCOPE 3)
(define TIPC_PUBLISHED 1)
(define TIPC_SRC_DROPPABLE 128)
(define TIPC_SUBSCR_TIMEOUT 3)
(define TIPC_SUB_CANCEL 4)
(define TIPC_SUB_PORTS 1)
(define TIPC_SUB_SERVICE 2)
(define TIPC_TOP_SRV 1)
(define TIPC_WAIT_FOREVER -1)
(define TIPC_WITHDRAWN 2)
(define TIPC_ZONE_SCOPE 1)

(define TCP_CONGESTION 13)
(define TCP_CORK 3)
(define TCP_DEFER_ACCEPT 9)
(define TCP_FASTOPEN 23)
(define TCP_INFO 11)
(define TCP_KEEPCNT 6)
(define TCP_KEEPIDLE 4)
(define TCP_KEEPINTVL 5)
(define TCP_LINGER2 8)
(define TCP_MAXSEG 2)
(define TCP_NODELAY 1)
(define TCP_QUICKACK 12)
(define TCP_SYNCNT 7)
(define TCP_USER_TIMEOUT 18)
(define TCP_WINDOW_CLAMP 10)

(define INADDR_ALLHOSTS_GROUP 3758096385)
(define INADDR_ANY 0)
(define INADDR_BROADCAST 4294967295)
(define INADDR_LOOPBACK 2130706433)
(define INADDR_MAX_LOCAL_GROUP 3758096639)
(define INADDR_NONE 4294967295)
(define INADDR_UNSPEC_GROUP 3758096384)

(define IPPROTO_AH 51)
(define IPPROTO_DSTOPTS 60)
(define IPPROTO_EGP 8)
(define IPPROTO_ESP 50)
(define IPPROTO_FRAGMENT 44)
(define IPPROTO_GRE 47)
(define IPPROTO_HOPOPTS 0)
(define IPPROTO_ICMP 1)
(define IPPROTO_ICMPV6 58)
(define IPPROTO_IDP 22)
(define IPPROTO_IGMP 2)
(define IPPROTO_IP 0)
(define IPPROTO_IPIP 4)
(define IPPROTO_IPV6 41)
(define IPPROTO_NONE 59)
(define IPPROTO_PIM 103)
(define IPPROTO_PUP 12)
(define IPPROTO_RAW 255)
(define IPPROTO_ROUTING 43)
(define IPPROTO_RSVP 46)
(define IPPROTO_SCTP 132)
(define IPPROTO_TCP 6)
(define IPPROTO_TP 29)
(define IPPROTO_UDP 17)

(define IP_ADD_MEMBERSHIP 35)
(define IP_DEFAULT_MULTICAST_LOOP 1)
(define IP_DEFAULT_MULTICAST_TTL 1)
(define IP_DROP_MEMBERSHIP 36)
(define IP_HDRINCL 3)
(define IP_MAX_MEMBERSHIPS 20)
(define IP_MULTICAST_IF 32)
(define IP_MULTICAST_LOOP 34)
(define IP_MULTICAST_TTL 33)
(define IP_OPTIONS 4)
(define IP_RECVOPTS 6)
(define IP_RECVRETOPTS 7)
(define IP_RETOPTS 7)
(define IP_TOS 1)
(define IP_TRANSPARENT 19)
(define IP_TTL 2)

(define IPV6_CHECKSUM 7)
(define IPV6_DONTFRAG 62)
(define IPV6_DSTOPTS 59)
(define IPV6_HOPLIMIT 52)
(define IPV6_HOPOPTS 54)
(define IPV6_JOIN_GROUP 20)
(define IPV6_LEAVE_GROUP 21)
(define IPV6_MULTICAST_HOPS 18)
(define IPV6_MULTICAST_IF 17)
(define IPV6_MULTICAST_LOOP 19)
(define IPV6_NEXTHOP 9)
(define IPV6_PATHMTU 61)
(define IPV6_PKTINFO 50)
(define IPV6_RECVDSTOPTS 58)
(define IPV6_RECVHOPLIMIT 51)
(define IPV6_RECVHOPOPTS 53)
(define IPV6_RECVPATHMTU 60)
(define IPV6_RECVPKTINFO 49)
(define IPV6_RECVRTHDR 56)
(define IPV6_RECVTCLASS 66)
(define IPV6_RTHDR 57)
(define IPV6_RTHDRDSTOPTS 55)
(define IPV6_RTHDR_TYPE_0 0)
(define IPV6_TCLASS 67)
(define IPV6_UNICAST_HOPS 16)
(define IPV6_V6ONLY 26)


(define AF_INET   (@ (guile) AF_INET))
(define AF_INET6  (@ (guile) AF_INET6))
(define AF_UNIX   (@ (guile) AF_UNIX))
(define AF_UNSPEC (@ (guile) AF_UNSPEC))

(define SOCK_CLOEXEC   (@ (guile) SOCK_CLOEXEC))
(define SOCK_NONBLOCK  (@ (guile) SOCK_NONBLOCK))
(define SOCK_RDM       (@ (guile) SOCK_RDM))
(define SOCK_STREAM    (@ (guile) SOCK_STREAM))
(define SOCK_DGRAM     (@ (guile) SOCK_DGRAM))
(define SOCK_RAW       (@ (guile) SOCK_RAW))
(define SOCK_SEQPACKET (@ (guile) SOCK_SEQPACKET))

(define MSG_DONTROUTE (@ (guile) MSG_DONTROUTE))
(define MSG_DONTWAIT  (@ (guile) MSG_DONTWAIT))
(define MSG_OOB       (@ (guile) MSG_OOB))
(define MSG_PEEK      (@ (guile) MSG_PEEK))
(define MSG_CMSG_CLOEXEC 1073741824)
(define MSG_CONFIRM 2048)
(define MSG_CTRUNC 8)
(define MSG_EOR 128)
(define MSG_ERRQUEUE 8192)
(define MSG_FASTOPEN 536870912)
(define MSG_MORE 32768)
(define MSG_NOSIGNAL 16384)
(define MSG_TRUNC 32)
(define MSG_WAITALL 256)

(define AI_ADDRCONFIG  (@ (guile) AI_ADDRCONFIG))
(define AI_CANONNAME   (@ (guile) AI_CANONNAME))
(define AI_NUMERICSERV (@ (guile) AI_NUMERICSERV))
(define AI_V4MAPPED    (@ (guile) AI_V4MAPPED))
(define AI_ALL         (@ (guile) AI_ALL))
(define AI_NUMERICHOST (@ (guile) AI_NUMERICHOST))
(define AI_PASSIVE     (@ (guile) AI_PASSIVE))

(define scm-socket     (@ (guile) socket))

(define-python-class error   (Exception))
(define-python-class timeout (Exception))

(define-python-class socket ()
  (define __init__
    (lam (self (= family AF_INET) (= type SOCK_STREAM) (= proto 0)
               (= fileno None))
      (set self '__family  family)
      (set self '__type    type)
      (set self '__proto   proto)
      (set self '__fileno
           (if (port? fileno)
               fileno
               (if (integer? fileno)
                   (fdes->ports fileno)
                   ((@ (guile) socket) family type proto))))))
                 
  
  (define _accept
    (lambda (self)
      (wrap self it
            (let ((r ((@ (guile) accept) it)))
              (values (car it) (cdr it))))))
             
  
  (define close
    (lambda (self)
      (wrap self it (close it))))

  (define connect
    (lambda (self addr)
      (catch #t
        (lambda ()
          (wrap self it
                (aif it ((@ (guile) connect) it (scm-str addr))
                     0
                     EAGAIN)
                EBADF))
        (lambda x EBADF))))
  
  (define dup
    (lambda (self)
      (wrap self it
            (dup (port->fdes it)))))
  
  (define getpeername
    (lambda (self)
      (wrap self it
            ((@ (guile) getpeername) it))))

  (define getsockname
    (lambda (self)
      (wrap self it
            ((@ (guile) getsockname) it))))

  (define getsockopt
    (lam (self level optname (= buflen 0))
      (wrap self it
            ((@ (guile) getsockopt) it level optname))))

  (define gettimeout
    (lambda (self) #f))

  (define listen
    (lambda* (self #:optional (backlog 5))
      (wrap self it
            ((@ (guile) listen) it backlog))))

  (define recv
    (lambda* (self buffersize #:optional (flags 0))
      (wrap self it
            (let ((buf (make-bytevector buffersize)))
              (let ((n (recv it buf flags)))
                (let ((r (make-bytevector n)))
                  (let lp ((i 0))
                    (if (< i n)
                        (bytevector-u8-set! r i (bytevector-u8-ref buf i))
                        (bytes r)))))))))


  (define recv_into
    (lambda* (self buffer #:optional (nbytes (len buffer)) (flags 0))
      (let ((r (recv self nbytes flags)))
        (let ((n (len r)))
          (let lp ((i 0))
            (if (< i n)
                (pylist-set! buffer i (pylist-ref r i))
                (values)))))))

  (define recvmsg
    (lambda* (self buffsize #:optional (ancbufsize 0) (flags 0))
      (error "recvmsg not implemented")))

  (define recvmsg_into
    (lambda* (self buffers #:optional (ancbufsize 0) (flags 0))
      (error "recvmsg not implemented")))

  (define send
    (lambda* (self data #:optional (flags 0))
      (wrap self it
            ((@ (guile) send) it (bv-scm data) flags))))

  (define sendall
    (lambda* (self data #:optional (flags 0))
      (let ((n (len data)))
        (let lp ((i 0) (b data))
          (if (= i n)
              (values)
              (let ((j (send self data flags)))
                (lp (+ i j) (pylist-slice i (+ i j) None))))))))
    

  (define sendmsg
    (lambda* (self buffers #:optional (ancdata 0) (flags 0) address None)
      (error "not sendmsg is not implemented")))

  (define sendmsg_afalg
    (lambda x
      (error "not sendmsg_afalg is not implemented")))

  (define sendto
    (case-lambda
      ((s data address)
       (sendto s data 0 address))
      ((s data flags address)
       (values))))

  (define setblocking
    (lambda (self flag)
      (if flag
          (settimeout self None)
          (settimeout self 0.0))))

  (define setsockopt
    (case-lambda
      ((self level option value) #f)
      ((self level option value optlen) #f)))
        
  (define settimeout
    (lambda (self time)
      #f))

  (define shutdown
    (lambda (self flag)
      (wrap self it
            ((@ (guile) shutdown) it flag)))))


(define (dup x)
  (if (port? x)
      (fdes->ports ((@ (guile) dup) (port->fdes x)))
      (fdes->ports ((@ (guile) dup) x))))

(define (getaddrinfo . l)
  (apply (@ (guile) getaddrinfo) l))

(define (getdefaulttimeout . l)
  (fluid-ref *default-time-out*))

(define (gethostbyname host)
  ((@ (guile) gethostbyname) host))

(define (gethostbyaddr addr)
  ((@ (guile) gethostbyname) addr))

(define (gethostbyname_ex host)
  (error "not implemented"))

(define (gethostname . l)
  (apply (@ (guile) gethostname) l))

(define (getnameinfo sockaddr flags)
  (error "not implemented"))

(define (getprotobyname name)
  ((@ (guile) getprotobyname) name))

(define* (getservbyname . l)
  (apply (@ (guile) getservbyname) l))

(define* (getservbyport . l)
  (apply (@ (guile) getservbyport) l))

(define (if_indextoname if_index)
  (error "not implemented"))

(define (if_nameindex)
  (error "not implemented"))

(define (if_nametoindex if_name)
  (error "not implemented"))

(define (inet_aton x)
  (inet-pton AF_INET x))

(define (inet_ntoa x)
  (inet-ntop AF_INET x))

(define (inet_ntop x)
  ((@ (guile) inet-ntop) x))

(define (inet_pton x y)
  ((@ (guile) inet-pton) x y))

(define (f32 i)
  (let ((bv (make-bytevector 4)))
    (bytevector-u32-set! bv 0 i 'big)
    (bytevector-u32-ref  bv 0 (native-endianness))))

(define (f16 i)
  (let ((bv (make-bytevector 4)))
    (bytevector-u16-set! bv 0 i 'big)
    (bytevector-u16-ref  bv 0 (native-endianness))))

(define (ntohl i) (f32 i))
(define (ntohs i) (f16 i))
(define (htonl i) (f32 i))
(define (htons i) (f16 i))

(define *default-time-out* (make-fluid None))
(define (setdefaulttimeout x)
  (fluid-set! *default-time-out* x))

(define (sethostname x)
  ((@ (guile) sethostname) (scm-str x)))




      




    


      




    
