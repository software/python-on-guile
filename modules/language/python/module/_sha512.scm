(define-module (language python module _sha512)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (sha512))

(define-python-class sha512 (Summer)
  (define name     "sha512")
  (define digest_size 64)
  
  (define _command "/usr/bin/sha512sum"))
