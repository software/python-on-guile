(define-module (language python module _blake2s)
  #:use-module (language python checksum)
  #:use-module (oop pf-objects)
  #:export (blake2s))

(define-python-class blake2s (Summer)
  (define name     "blake2s")
  (define digest_size 32)
  
  (define _command "/usr/bin/blake2ssum"))
